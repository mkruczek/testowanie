package pl.sdacademy.testowanie;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-08-05.
 */
public class WalutowyKonwerterTest {

    WalutowyKonwerter walutowyKonwerter;

    @Before
    public void setUp() throws Exception {
        walutowyKonwerter = new WalutowyKonwerter(new BigDecimal(2.5));
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testujKonwerter() {
        BigDecimal first = new BigDecimal(4);

        String result = String.valueOf(walutowyKonwerter.przeliczWalute(first));

        assertTrue(result.compareTo("10.0") == 0);
    }

    @Test
    public void testujNullowoscWaluty() {
        WalutowyKonwerter walutowyKonwerter = new WalutowyKonwerter(new BigDecimal(23));

        assertNotNull(walutowyKonwerter);
    }

    @Test
    public void testujCzyJestPustaLista() {

        List<Waluta> listaZMetody = walutowyKonwerter.getListaWalut();
        Waluta[] walutyZMetody = new Waluta[listaZMetody.size()];

        for (int i = 0; i < listaZMetody.size(); i++) {
            walutyZMetody[i] = listaZMetody.get(i);
        }

        Waluta[] listaDoPorównania = new Waluta[0];

        assertArrayEquals(listaDoPorównania, walutyZMetody);

    }

    @Test
    public void czyListMa3Elementy(){
        walutowyKonwerter.getListaWalut().add(new Waluta("USD", new BigDecimal("3.99")));
        walutowyKonwerter.getListaWalut().add(new Waluta("EUR", new BigDecimal("4.18")));
        walutowyKonwerter.getListaWalut().add(new Waluta("PLN", new BigDecimal("1.00")));

        assertEquals(3, walutowyKonwerter.getListaWalut().size());


    }

    @Test
    public void czyKonstruktorJestNullem(){
        //
        WalutowyKonwerter walotwyKonwerter = new WalutowyKonwerter(null);

        assertNotNull(walotwyKonwerter);

    }

}