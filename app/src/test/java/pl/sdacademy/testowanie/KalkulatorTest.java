package pl.sdacademy.testowanie;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.regex.Matcher;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-08-05.
 */
public class KalkulatorTest {

    Kalkulator kalkulator;


    @Before
    public void setUp() throws Exception {

        kalkulator = new Kalkulator();

    }

    @After
    public void tearDown() throws Exception {

        kalkulator = null;
    }

    @Test
    public void testeujDodawanie(){
        assertEquals(7, kalkulator.dodawanie(3,4));
    }

    @Test
    public void testujOdejmowanie(){
        assertEquals(4, kalkulator.odejmowanie(9,5));
    }

    @Test
    public void testujMnożenie(){
        assertEquals(36, kalkulator.mnozenie(9, 4));
    }

    @Test
    public void testujDzielenie(){

        BigDecimal first = new BigDecimal(11);
        BigDecimal second = new BigDecimal(3);

        String result = String.valueOf(kalkulator.dzielenie(first, second));

        assertTrue(result.compareTo("3.666666666666667") == 0);
    }


}