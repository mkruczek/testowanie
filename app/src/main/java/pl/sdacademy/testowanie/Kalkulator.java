package pl.sdacademy.testowanie;

import android.widget.TextView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by RENT on 2017-08-05.
 */

public class Kalkulator {



    public int dodawanie(int a, int b) {
        return a + b;
    }

    public int odejmowanie(int a, int b) {
        return a - b;
    }

    public int mnozenie(int a, int b) {
        return a * b;
    }

    public BigDecimal dzielenie(BigDecimal a, BigDecimal b) {
        BigDecimal result = new BigDecimal(0);
        result = a.divide(b, MathContext.DECIMAL64);
        return result;
    }


}
