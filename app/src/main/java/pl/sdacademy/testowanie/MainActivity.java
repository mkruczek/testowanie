package pl.sdacademy.testowanie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    TextView wynik;
    EditText pierwsza;
    EditText druga;
    Button dod;
    Button min;
    Button mnoz;
    Button dziel;
    Button konwert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wynik = (TextView) findViewById(R.id.wynik);
        pierwsza = (EditText) findViewById(R.id.pierwsza);
        druga = (EditText) findViewById(R.id.druga);
        dod = (Button) findViewById(R.id.dodawanie);
        min = (Button) findViewById(R.id.odejmowanie);
        mnoz = (Button) findViewById(R.id.mnozenie);
        dziel = (Button) findViewById(R.id.dzielenie);
        konwert = (Button) findViewById(R.id.konwerter);

        final Kalkulator kalkulator = new Kalkulator();
        final WalutowyKonwerter walutowyKonwerter = new WalutowyKonwerter(new BigDecimal(2.5));

        dod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int first = Integer.valueOf(pierwsza.getText().toString());
                int second = Integer.valueOf(druga.getText().toString());

                int result = kalkulator.dodawanie(first , second);

                wynik.setText(String.valueOf(result));
            }
        });

        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int first = Integer.valueOf(pierwsza.getText().toString());
                int second = Integer.valueOf(druga.getText().toString());

                int result = kalkulator.odejmowanie(first , second);

                wynik.setText(String.valueOf(result));
            }
        });

        mnoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int first = Integer.valueOf(pierwsza.getText().toString());
                int second = Integer.valueOf(druga.getText().toString());

                int result = kalkulator.mnozenie(first , second);

                wynik.setText(String.valueOf(result));
            }
        });

        dziel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(brakWartosci(pierwsza.getText().toString(), druga.getText().toString() )) {
                    BigDecimal first = new BigDecimal(pierwsza.getText().toString());
                    BigDecimal second = new BigDecimal (druga.getText().toString());

                    BigDecimal result = kalkulator.dzielenie(first , second);
                    wynik.setText(String.valueOf(result));
                } else {
                    printError("brak parametru");
                }



            }
        });

        konwert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BigDecimal value = new BigDecimal(wynik.getText().toString());

                String str = String.valueOf(walutowyKonwerter.przeliczWalute(value));

                wynik.setText(str);


            }
        });


    }

    public boolean brakWartosci(String a, String b){
        boolean result = true;
        if( a.equals("") || b.equals("")){
            result = false;
        }

        return result;
    }

    public void printError(String str){
        wynik.setText(str);
    }
}
