package pl.sdacademy.testowanie;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-08-05.
 */

public class WalutowyKonwerter {

    private BigDecimal wartosciWaluty;
    private List<Waluta> listaWalut;

    public WalutowyKonwerter(BigDecimal wartosciWaluty) {
        this.wartosciWaluty = wartosciWaluty;
        this.listaWalut = new ArrayList<>();
    }

    public List<Waluta> getListaWalut() {
        return listaWalut;
    }

    public BigDecimal przeliczWalute (BigDecimal value){
        BigDecimal result = new BigDecimal(0);
        result = wartosciWaluty.multiply(value);

        return result;
    }

    public void addWaluta(Waluta waluta){
        listaWalut.add(waluta);
    }

}
