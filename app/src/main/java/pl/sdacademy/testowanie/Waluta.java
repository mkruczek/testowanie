package pl.sdacademy.testowanie;

import java.math.BigDecimal;

/**
 * Created by RENT on 2017-08-05.
 */

public class Waluta {

    private String name;
    private BigDecimal value;

    public Waluta(String name, BigDecimal value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
